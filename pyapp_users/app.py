from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UserAppConfig(AppConfig):
    name = 'pyapp_users'
    verbose_name = _('users')
