from __future__ import absolute_import, unicode_literals
import logging
from celery import shared_task

from pyapp_users.utils import get_verification_function, send_vas_verification
from django.contrib.auth import get_user_model

logger = logging.getLogger('pyapp.users')
User = get_user_model()


@shared_task
def sending_vas_verification_message(phone_number, device_uuid, device_model, device_type):
    """
    Sending SMS verification through VAS service
    :param phone_number: the incoming phone number that send sms verification
    :param device_uuid: device id
    :param device_model: device model for an example nokia
    :param device_type: 1 => phone device 2 => browser
    """
    data = {'phone_number': phone_number, 'device_uuid': device_uuid,
            'device_model': device_model, 'device_type': device_type}
    result = send_vas_verification(data)
    if result is None:
        return result

    user = User.objects.filter(phone_number=phone_number).first()
    if user is None:
        logger.error('user {0} not found. in sending verification vas '.format(phone_number))

    user_device = user.devices.filter(device_uuid=device_uuid).first()

    if user_device is None:
        logger.error('user device {0} not found. in sending verification vas '.format(device_uuid))

    user_device.register_code = result['register_code']
    user_device.save()


@shared_task
def sending_sms_verification(phone_number, verify_code):
    """
    Sending SMS verification through ADP service
    :param phone_number: the incoming phone number
    :param verify_code: verification code that send to the phone number
    """
    send_verification_func = get_verification_function()
    send_verification_func(phone_number, verify_code)
