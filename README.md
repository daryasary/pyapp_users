# Welcome to PyApp Users

PyApp Users is a pluggable user registration and auth(entication|orization) app for (Yara)django projects

## Prerequisites

**Note**: It is always recommended to use a [virtualenv](https://virtualenv.pypa.io/en/stable/) while developing Python applications.

## Caution
> after release 0.13 the app does not backward compatible

## Getting Started

1. First of all, you need to install the package:

    By setting SSH keys in gitlab, you can connect to gitlab without supplying your username or password at each visit(Recommanded):
    ```
    -> pip install git+ssh://git@gitlab.com/arash77/pyapp-users.git
    ```
    also with https:
    ```
    -> pip install git+https://gitlab.com/arash77/pyapp-users.git

    ```
    
    or download file from https://gitlab.com/arash77/pyapp-users/raw/master/dist/pyapp_users-xyz.tar.gz and run:
    
    ```
    -> pip install path/to/downloaded/pyapp_users-xyz.tar.gz
    ```

2. Add `pyapp_users` to your INSTALLED_APPS setting like this (`settings.py`):

    ```
    INSTALLED_APPS = [
            ...
            'rest_framework', # for documentations
            'pyapp_users',
        ]
    
    ```

3. Override the default user model by providing `pyapp_users.User` for the AUTH_USER_MODEL setting (`settings.py`):

    ```
    AUTH_USER_MODEL = 'pyapp_users.User'
    ```

4. Specifying authentication backends
    * if you need SMS verification, add `pyapp_users.backends.SMSBackend` to AUTHENTICATION_BACKENDS
    * if you need Google verification, add `pyapp_users.backends.GoogleOAuthBackend` to AUTHENTICATION_BACKENDS
    * if you need VAS verification, add `pyapp_users.backends.VASBackend` to AUTHENTICATION_BACKENDS
    
    for an example, in our project we need all of them (`settings.py`):
    
    ```
    AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.ModelBackend',
        'pyapp_users.backends.SMSBackend',
        'pyapp_users.backends.GoogleOAuthBackend',
        'pyapp_users.backends.VASBackend',
        ...
    ]
    ```

5. There are some JWT additional settings that will be set (`settings.py`):

    Short-lived access token (e.g < 1h) will be enough to hurt the authentication service's performance.
    also long-lived access token (e.g > 2week) has security issue (token hijacking) 
    
    ```
    import datetime
    
    JWT_AUTH = {
        'JWT_ALLOW_REFRESH': True,
        'JWT_EXPIRATION_DELTA': datetime.timedelta(days=7),
        'JWT_RESPONSE_PAYLOAD_HANDLER': 'pyapp_users.utils.jwt_response_payload_handler',
        'JWT_PAYLOAD_HANDLER': 'pyapp_users.utils.jwt_payload_extender',
    }
    ```

6. There are some additional settings that you can override similar to how you'd do it with Django REST framework itself. Here are all the available defaults.

    ```
    PYAPP_USERS = {
        'VAS_SERVICE_ID': 0,        # 0 => default registration else => VAS registration
        'VAS_BASE_URL': 'http://vasp.yaramobile.com/api',        # VAS registration server
        'VAS_REGISTER_URL': '{BASE_URL}/{SERVICE_ID}/devices/',        # VAS registration URI
        'VAS_VERIFY_URL': '{BASE_URL}/verify/{REGISTER_CODE}/{VERIFY_CODE}/',        # VAS SMS verification URI
        'VERIFICATION_TEMPLATE_PATH': None        # Required - Verification message template path, it must be in templates dir
        'FINOTECH_SERVICE_ID': 0,
        'FINOTECH_TOKEN_URL': '',
        'FINOTECH_VERIFY_URL': '',
        'SITE_URL': 'http://localhost:8000', # Required -> for email verification link
        'EMAIL_VERIFICATION_URI': 'auth/manage/user-email/verify',
        'VERIFY_CODE_MIN': 10000,
        'VERIFY_CODE_MAX': 99999,
        'EMAIL_VERIFICATION_TEMPLATE_PATH': None, # Required -> for email verification template, it must be in templates dir
        'EMAIL_VERIFICATION_SUBJECT': 'اعتبار سنجی ایمیل',
        'SMS_VERIFICATION_HANDLER': None  # Required => must be set
    }
    ```

7. setting `SMS_VERIFICATION_HANDLER`:

    **Example**: a sms verification handler example:
    ```
    # project.utils
    def send_verification(phone_number, verify_code):
        message = render_to_string('text/phone_verify.txt', {
            'verify_code': verify_code
        })
    
        query_params = {
            'username': 'username',
            'password': 'password',
            'srcaddress': '9800000000',
            'dstaddress': phone_number,
            'body': message,
            'unicode': 1,
        }
    
        try:
            response = requests.get('http://ws.adpdigital.com/url/send',
                                    params=query_params, timeout=60)
    
            response.raise_for_status()
    
            if response.ok and 'ERR' in response.text:
                print('ADP verification failed: {0}'.format(query_params))
                raise AuthenticationFailed(detail='Server authentication failed')
    
        except Exception as e:
            print('Sending verification code to {0} failed: {1}'.format(phone_number, e))
            raise ValidationError({'detail': 'Could not sent SMS verification'})
    ```
    and set `SMS_VERIFICATION_HANDLER` in settings to `project.utils.send_verification`:
    ```
    PYAPP_USERS = {
        'VAS_SERVICE_ID': 0, # must be set to zero when using custom sms verification handler
        ...
        'SMS_VERIFICATION_HANDLER': 'project.utils.send_verification'
    }
    ```
    > **New**: in 0.11
    
    **Note**: you can use pyapp_users `send_verification`:
    ```
    PYAPP_USERS = {
        'VAS_SERVICE_ID': 0, # must be set to zero when using custom sms verification handler
        ...
        'SMS_VERIFICATION_HANDLER': 'pyapp_users.utils.send_verification'
    }
    ```
    you must add bellow settings to **project** settings:
    
    ```
    VERIFICATION_TEMPLATE_PATH = 'text/phone_verify.txt'
    VERIFICATION_USERNAME = 'username'
    VERIFICATION_PASSWORD = 'password'
    VERIFICATION_SMS_NUMBER = 'sms_number'
    VERIFICATION_URI = 'http://verifications-server-address'
    UNICODE = 1
    ```
    
    > **New**: in 0.12

7. **Deprecated**: you don't need this settings any more

    In your `settings.py`, add `JSONWebTokenAuthentication` to Django REST framework's `DEFAULT_AUTHENTICATION_CLASSES`.

    ```
    REST_FRAMEWORK = {
        ...
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
            ...
        ),
    }
    ```

8. Adding throttling to the rest framework for user registration(`settings.py`):

    ```
    REST_FRAMEWORK = {
        ...
        'DEFAULT_THROTTLE_RATES': {
            'anon': '5/minute',
            'user': '5/minute'
        }
    }
    ```

8. add log configuration in `settings.py`.

    **Note**: You must have a logger named `pyapp.users`
    
    Here's an example:
        
    ```
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                'class': 'django.utils.log.AdminEmailHandler'
            },
            'null': {
                'level':'DEBUG',
                'class':'logging.NullHandler',
            },
            'console':{
                'level':'DEBUG',
                'class':'logging.StreamHandler',
                'formatter': 'verbose'
            },
            'logfile': {
                'level':'DEBUG',
                'class':'logging.handlers.RotatingFileHandler',
                'filename': os.path.join(BASE_DIR, 'current.log'),
                'maxBytes': 1024*1024*5, # 5MB
                'backupCount': 0,
                'formatter': 'verbose',
            },
        },
        'formatters': {
            'verbose': {
                'format': '%(levelname)s|%(asctime)s|%(pathname)s|%(module)s|%(process)d|%(thread)d|%(filename)s:%(lineno)d|%(message)s',
                'datefmt' : "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s|%(message)s'
            },
        },
        'loggers': {
            'app_name.request': {
                'handlers': ['mail_admins'],
                'level': 'ERROR',
                'propagate': True,
            },
            'app_name.tasks': {
                'handlers': ['mail_admins'],
                'level': 'ERROR',
                'propagate': True,
            },
            'app_name.management': {
                'handlers': ['console', 'logfile'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'pyapp.users': { # <== Required
                'handlers': ['console', 'logfile'],
                'level': 'DEBUG',
                'propagate': True,
            },
        }
    }
    ```

9.  Open the file `project_name/urls.py` and put the following Python code in it:

    ```
    from pyapp_users import urls
    ...
    
    urlpatterns = [
        ...
        url(r'^auth/', include(urls))
    ]
    
    ```

10. Add `celery.py` file to your project package(If you have not already added) and add the following python code in it (**change** `project_name` to your project name):

    ```
    from __future__ import absolute_import
    import os
    from celery import Celery
    
    
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project_name.settings')
    
    from django.conf import settings
    
    app = Celery(broker=settings.CELERY_BROKER_URL)
    app.config_from_object('django.conf:settings')
    app.autodiscover_tasks(settings.INSTALLED_APPS)
    
    
    if __name__ == '__main__':
        app.start()
    ```

11. set CELERY_BROKER_URL up in `settings.py`(If you have not already added):
    ```
    CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/'
    ```

12. We need to create the tables in the database before we can use them. To do that, run the following command:

    ```
    -> python manage.py migrate
    
    ```

13. Run the following command to create super user:

    ```
    -> python manage.py createsuperuser --username hadi --email hadi@email.com --phone_number 9899100101
    
    ```

14. now run the project:

    ```
    -> python manage.py runserver
    ```

15. Run RabbitMQ in the background (If you have not already run it)

    ```
    -> sudo rabbitmq-server -detached
    ```

16. Running celery app (project root directory)(**change** `project_name` to your project name):

    ```
    -> celery -A project_name worker -l info
    ```


## API Documentation

Open the http://127.0.0.1:8000/auth/docs/ url in a browser and view api docs


## Cautions

### Refresh Token

http://127.0.0.1:8000/auth/refresh-token/

1. Each token is refreshed only once
2. Only expired token are refreshed


If you get the following message:

> Token is still valid

you must continue with current token

If you get the following message:

> The token is invalid

user must login again


## Change user email

After user registration, the user may want to change his/her email for authentication

1. Add `djcelery` and `djcelery_email`  to your INSTALLED_APPS setting like this (`settings.py`):

    ```
    INSTALLED_APPS = [
            ...
            'rest_framework', # for documentations
            'pyapp_users',
            'djcelery',
            'djcelery_email',
        ]
    
    ```

2. Override some PYAPP_USERS settings(`settings.py`), for an example:

    ```
    PYAPP_USERS = {
        ...
        'SITE_URL': 'http://localhost:8000'
        'EMAIL_VERIFICATION_TEMPLATE_PATH': os.path.join(TEXT_TEMPLATE_DIR, 'email_verification.html'),
        'EMAIL_VERIFICATION_SUBJECT': 'اعتبارسنجی ایمیل'
    }
    ```

3. Add `celery.py` file to your project package and add the following python code in it:

    ```
    from __future__ import absolute_import
    import os
    from celery import Celery
    
    
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project_name.settings')
    
    from django.conf import settings
    
    app = Celery(broker=settings.CELERY_BROKER_URL)
    app.config_from_object('django.conf:settings')
    app.autodiscover_tasks(settings.INSTALLED_APPS)
    
    
    if __name__ == '__main__':
        app.start()
    ```

4. set CELERY_BROKER_URL up in `settings.py`:

    ```
    CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/'
    ```

5. Set django-celery-email as project’s email backend, and the SMTP server configurations, go to `settings.py` and add the following line.

    ```
    EMAIL_HOST = 'SMTP_HOST'
    

    EMAIL_PORT = 'SMTP_PORT'
    
    
    EMAIL_HOST_USER = 'SMTP_USER'
    
    
    EMAIL_HOST_PASSWORD = 'SMTP_PASSWORD'
    
    
    EMAIL_USE_TLS = True # TLS settings
    
    
    EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'
    ```

6. Run RabbitMQ in the background.

    ```
    -> sudo rabbitmq-server -detached
    ```

7. Running celery app (project root directory):

    ```
    -> celery -A project_name worker -l info
    ```

## Change user phone number
After user registration, the user may want to change his/her phone for authentication if condition passed(set `'CAN_CHANGE_PHONE_NUMBER': False` in setting or VAS User registration Service does not provided)

1. Override some PYAPP_USERS settings(`settings.py`), for an example:

    ```
    PYAPP_USERS = {
        ...
        'VAS_SERVICE_ID': 0,
        'VERIFICATION_TEMPLATE_PATH': os.path.join(TEXT_TEMPLATE_DIR, 'phone_verify.txt'),
        'EMAIL_VERIFICATION_TEMPLATE_PATH': os.path.join(TEXT_TEMPLATE_DIR, 'email_verification.html'),
        'EMAIL_VERIFICATION_SUBJECT': 'اعتبارسنجی ایمیل',
        'CAN_CHANGE_PHONE_NUMBER': True
    }
    ```

2. Add `celery.py` file to your project package(If you have not already added) and add the following python code in it:

    ```
    from __future__ import absolute_import
    import os
    from celery import Celery
    
    
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project_name.settings')
    
    from django.conf import settings
    
    app = Celery(broker=settings.CELERY_BROKER_URL)
    app.config_from_object('django.conf:settings')
    app.autodiscover_tasks(settings.INSTALLED_APPS)
    
    
    if __name__ == '__main__':
        app.start()
    ```

3. set CELERY_BROKER_URL up in `settings.py`(If you have not already added):
    ```
    CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/'
    ```

4. Run RabbitMQ in the background (If you have not already added)

    ```
    -> sudo rabbitmq-server -detached
    ```

5. Running celery app (project root directory):

    ```
    -> celery -A project_name worker -l info
    ```

## Phone number encryption

for encrypting phone number in user registration we use AES(Advanced Encryption Standard).

for encryption you can write your own encryption-decryption class and set it in pyapp_users settings:

`settings.py`
```
PYAPP_USERS = {
        ...
        'ENCRYPTION_CLASS': 'pyapp_users.utils.AESCipher',
        'ENCRYPTION_KEY': None ==> Required
    }
```

**Note**: `ENCRYPTION_KEY` must be set, it is a key for encryption and decryption

**Note**: `ENCRYPTION_CLASS` must be set, if you don't set it, by default uses `pyapp_users.utils.AESCipher`

### Client Guidline for encryption

if you use default `ENCRYPTION_CLASS` then you must follow this guild line to encrypt the phone number

1. first of all we need a key, this key stored in settings file(`ENCRYPTION_KEY` explained above), this key must be hash with sha256 (python code):

    ```
    key = hashlib.sha256(key.encode()).digest()
    ``` 
2. we need a 16 character string, name it `IV` stand for Initialization Vector (python code):
    
    ```
    from Crypto import Random
    
    iv = Random.new().read(AES.block_size)  # AES block_size is 16
    ``` 
3. then adding padding to the phone_number that length must be a multiple of 16, convert bellow solution to your language (python code):
    
    ```
    length = 16
    pad_phone_number = phone_number + (length - len(phone_number) % length) * chr(length - len(phone_number) % length)
    ```
4. create an object of AES type and set mode to CBC (python code):

    ```
    cipher = AES.new(key, AES.MODE_CBC, iv)
    ```
5. and finally decrypt the result and also encode with base64 (python code):

    ```
    enc_phone_number = base64.b64encode(iv + cipher.encrypt(pad_phone_number))
    ```
