from setuptools import setup, find_packages

setup(name='pyapp_users',
      version='0.18.1',
      description='pyapp pyapp_users package',
      author='pyapp pyapp_users',
      author_email='pyapp_users@example.com',
      license='MIT',
      packages=find_packages(),
      data_files=[('locale', ['pyapp_users/locale/fa/LC_MESSAGES/django.mo', 'pyapp_users/locale/fa/LC_MESSAGES/django.po'])],
      install_requires=[
          'Django>=1.11.9, <2.0',
          'requests',
          'Pillow',
          'djangorestframework>=3.6.4',
          'oauth2client',
          'djangorestframework-jwt>=1.8.0',
          'coreapi==2.3.3',
          'celery',
          'django-celery-email'
      ],
      include_package_data=True,
      zip_safe=False)
